# Software testing

There is no one way to test software, but there is a standard set by ISTQB. That we can follow. A simple standard would be:

* Plan - Analyze
* Design test cases - make scenarios / scripts for automated testing
* Writing of test reports, defect tracking, defect analysis

## 7 Test process activities:

* Planning - Test plan
* Monitoring & Control - While project is going, monitor the progress to the progress written in the project. If there is a difference in progress we perform control activities - corrective activities in order to follow our plan.
* Analyse the requierments 
* test design, write test cases based on the analysis
* test implementation, done in testing environment
* test execution, do tests, retests,...
* test completion, theoretical reports

## Test levels

### Done by the developers:

* Unit (aka component) testing: testing of individual modules 

### Done by both teams:

*Integration testing*: this means testing all the modules together, interaction between multiple modules. This also consists of 2 seperate testings:

* component integration testing: done by the developers, to test that the modules that directly interact with eachother work as expected.
* system integration testing: consisting of modules together that create a working sub-system (front end, back end, some services etc). This is done by the tester.

### Only done by Software tester

Full system testing: Testing the system as a whole, front and back end. The most work for the software tester happends here. Test the software on all devices that it will be used on in production.

Acceptance testing is similar to system testing, but we try to find as many defects as possible. This can be done by stakeholders and or users. Mostly done in alpha/beta stages of software. 

## Testing types

There are alot of ways to testing (+100 ways). Learning all of these is  pretty useless, as it is mostly different names for the same thing. 

The important types are:

* Functional testing: testing main functions of the software. Ex; login, registration functionality. This is mostly answerd with yes or no. Ex: does the log in work yes or no.

* Non-functional testing: Testing how the system performs. Can usually not answer with yes or no, but is answered with a range, ex: took 9 seconds to login. These are parts of the tests that are done with tools.

* Black box testing: Testing without knowing the internal structure of the system. Not looking at what the code is doing.

* White box testing: you provide input, and look at the internal actions based on your input.

* Dynamic testing: Testing that includes executing the software.

* Static testing: testing while not executing anything in teh software. ex: reviewing requierments, user requirements.

* Retesting (confirmation testing): we committed a bug report and the developers have fixed it. We do the same steps to reproduce the defect.

* Regression Testing: The dev added / removed / changed a feature, we will test the unchanged parts of the software. This is done alot with automated tests.

* Smoke testing: An application with alot of features. Basic tests get done on the features, and we let it trough. (stating it is "stable enough") This test will take 3 hours or so, not 3 days. If the basic testing fails, it is returned to the developer.

******
## Test case writing

Most important document that you write as software tester. For ex: you need to test login functionality. You need to cover case possible. You first write all these possibilities down, and then go ahead and start testing.

In the beginning we will explain an easy test case.

A test case is a set of preconditions inputs actions (where applicable) expected results and postconditions developed based on the application.

## Test case contents:

### Test case title:

The title needs to be simple, it should be clear as to what you are testing. For ex: testing login with email, testing login with username etc... 

### Precondition

This condition must be met before we can test it. Ex: to login we need to register before we can login, so we must already have a valid user registered.


### Test steps

These must be very clear, but it leaves out the intuitive steps (connect to wifi, open browser,...).

EX of high level test case:

	1) Enter a valid username
	
	2) Enter a valid password
	
	3) Click on sign in


A lower level cases you would do it like:

	1) use this username
	2) use this password
	3) login


The difference between the two is:

**High level test cases:** are written when there is no software yet to test. **Low level test cases** get written when there is some software to test, or a specific feature to test.

### Expected result

EX: User is logged in and redirected to the expected page.

- You should define what the expected result / page is 

### Test scenario (test suite)

This is where the test case belongs to. If a project is small we can add all the login test cases inside one test suite. Else we would divide them up into categories (multiple suites) like login,...

### Test environment

What is the device we test this on, for ex: the hardware (screen size, specs of pc), software (windows, android, linux, ios,...), network: should it be tested on mobile, wifi or cabled internet connection.

Ex: Windows 10 - chome - wifi, samsung galaxy note 10 - android 10 - 4g network

### Actual result

These are filled in after executing the test case. Dont write this before executing the test case. 

You always write the field during the designing of the test case, but you leave it empty for later

### Status

- new: Test case is not executed
- pass: the test case is executed and the actual result is the same as the expected result
- fail: the test case is executed and the actual result is different from the expected result
- blocked/skipped: the test case cant be executed(ex: a login is not failed if the functionality is not there.)


### Example of test case:

![](example-of-test-case.png)

## Writing bug reports

### defining of a bug report

It is a document of the cocurence,nature and status of a defect.
This is a imperfection or deficiency in a work product where it does not meet its requierments or specifications. A bug,fault and defect are synonyms.

### bug report title

section -> description
for example, in the register area:
register -> no error message appears when user leaves password field empty.

The title makes you understand what and where the problem is. 

### Steps to reproduce

* Must be very specific
* ex:
	1) open www.facebook.com
	2) click on the hamburger icon in the upper left corner
	3) scroll down to the bottom of the screen
	4) click on settings
	5) click on data usage
	6) change data usage to minimum limit

### Actual result

* what really happened when the steps are executed

### test environment

Specify the environments in which the defect occured.

* windows 10 - chrome - wifi
* samsung galaxy note 10 android 10 4g network
* iphone 11 ios13.3.1 5g network

You can also specify the rate of which it happends, does it always happen or is it 1 out of 5 times.

### screenshot or video

* the screenshot must show the whole screen
* a red rectangle should be around the defect area 
* the video should show the clicks on the keyboard

a video is also handy if you want to show the loading times of the application.
in both, the time and date the test was done is also handy to include.

### bug priority

there are 4 bug priorities that you can use, here they are incl some examples.

* critical: login isnt working, crashes in home page, wrong cart value
* high: login page responds slowly, user not able to add profile image
* medium: some pages have poor performance - portrait mode isnt working correctly.
* low: spelling mistakes - image misalignment.

## types of defects

1) functional defects

if there is a functionality that is not working. ex: the forgot password is not working

2) visual defects

ex: the positioning of elements is wrong.

3) content defects

this can be misspelling, or wrong words (forgotten placeholders,...)

4) performance defects

ex: a video takes too much time to start playing. Or lazy loading

5) suggestion

this is not a defect, thedeveloper does not need to solve it.

ex: a larger font.

![](example-defect-report.png)


**note: there are ways to better document these reports in tools like trello or jira, but this will be covered later in the course.**

# Testing with trello

You can call the lists for example test scenarios, new defects, resolved defects.

![](trello-testing.png)

Inside the lists you can add cards. You can set here the functional requierments (FR) title. For example: FR1 Download mobile app. FR2 Notify users of new releases

You can use the checklists inside the cards to set the steps. (the test requierments)

example:

A skilled tester will think of many scenarios to test with, if we have time  we can add some more.

you can copy and paste the description of the feature in the card's description

![](test-scenario-card.png)

for large environments, it might be usefull to split up boards. for example: restaurant owner - administrator - user. 

## priority ordering

In the software requierments doc there will be a priority ranking. If there is not, it will be done out of experience of the tester.

***

# Test reports

This helps to visualize the quality of the product in the current state.

* test progress report: a type of test report produced at regular interval about the progress of test activities against a baseline, risks, and alternatives requiring a decision. Synonyms: test status report.

A test progress report can be done each week, to give stakeholders feedback on the quality of the software. 

* test summary report: a type of test report prouced at the _completion milestones_ that provides an evaluation of the corresponding test items against exit criteria.

When a unit test finishes, performance testing is done, etc,... everytime a completion milestone is hit, a test summary can be made.

There is no real template for writing templates. It depends on who the reports are for. A stakeholder and a direct customer will have a different version.

Most of the time, you can ask the intended audience "what do you want to know" about the software and you can make a template around that.

A sample of a test report for stakeholders can be found [here](https://docs.google.com/document/d/1PmayrShEgUoz-ZNlY9gP2Fi1Qj1RJ-aE7cRnlpH0nng/edit)

***

# Black box testing techniques

Testing the system without knowing the internal structure of the system. You provide inputs and wait for some outputs.

For ex: testing a calculator, if you input 2+3 you expect output to be 5. If output is 8, it is a bug.

There are 4 major techniques in black box testing. 

* equivalance partitioning
	- divide the system to seperate partitions based on some criteria.
* boundary value analysis
	- analyze the system at boundary values
* decision table testing
* state transition testing

# Equivalance partitioning

The system is partitioned, based on criteria.

At each partition you need 1 test w/ an expected result. 

Ex:							
						
							expected result
							
	partition 1 = 11 am		at work
	partition 2 = 4 pm		social media
	partition 3 = 6 pm		study
	partition 4 = 7:30 pm	break
	partition 5 = 9:30 pm	study
	partition 6 = 11:00 pm	sleep
	

These are 6 test cases for 6 partitions. If everything comes out with the expected result, it is 100% EP (Equivalance partitioning).

Why is it useful? A tester tests a system and needs to find bugs / errors. He will divide everything up into partitions and tests partition by partition.

We divide the whole system up and then make the test cases.

## Equivalence partitioning example 1:

You are testing a machine that scores exam papers and assigns grades. Based on the score achieved the grades are as follows:

	1-49: f
	50-59: d-
	60-69: d
	70-79: c
	80-89: b
	90-100: a

If you apply equivalence partitioning, how many test cases will you need?

There are 6 partitions

	partition 1 - 1-49: f
	partition 2 - 50-59: d-
	partition 3 - 60-69: d
	partition 4 - 70-79: c
	partition 5 - 80-89: b
	partition 6 - 90-100: a
	
So you can test at for example
	
	25
	55
	62
	77
	81
	99

But you should also test what comes before 1, and 100. 

so **result is 8 test cases**. invalid partitions indicate abnormal behavior, but it has to be tested none the less.

	-3		invalid partition
	25		valid partition
	55		valid partition
	62		valid partition
	77		valid partition
	81		valid partition
	99		valid partition
	102		invalid partition
	

## Equivalence partitioning example 2

Registering for a website:

* age < 13
	- an error message telling the user that he is too young to join this website
* age between 13 and 100
	- a message appears to the user welcoming him to the website and asking him to complete the process
* invalid value for age
	- an error message appears telling the user that he must enter a valid value for age.

a valid value might be 25. Or age lower then 13 (10)

invalid examples: -3, 150, "three".

There are 4 partitions you can have. It depends on the amount of time you have, and the type of system you are testing.

## Applying EP to facebook registration

(video in repo)

# Boundary value analysis

It is an extension on EP. In EP we will test @ any value inside the partition. Boundary value analysis will test and the beginning of the partition and at the end. Testing at the breaking points of the system.

There is 2 types of analysis, 2 value and 3 value analysis. In 2 value analysis you must know the boundary exactly. 

For example if you need to be atleast age 13 to register. 

13 is the beginning of a valid partition.
12 if the end of the invalid partition. 

If you use 3 value boundary value analysis, you would test on 13,12 and 14.

## Example 1

You are testing a machine that scores exam papers and assigns grades. Based on the score achieved the grades are as follows:

	1-49: f
	50-59: d-
	60-69: d
	70-79: c
	80-89: b
	90-100: a

If you apply boundary value analysis, how many test cases will you need?

There are 6 partitions

	partition 1 - 1-49: f
	partition 2 - 50-59: d-
	partition 3 - 60-69: d
	partition 4 - 70-79: c
	partition 5 - 80-89: b
	partition 6 - 90-100: a
	
So you can test at these values (w/ 2 value boundary analysis)
	
	0
	1-49
	50-59
	60-69
	70-79
	80-89
	90-100

## Notes on EP and Boundary-Value Analysis

These are important to understanding the techniques in a better way.

1) each value belongs only to one partition

you can not have one value that is between values. If this is the case, it would be a different partition

2) Equivalence partitioning can be applied to both input or output values.

3) Boundary-Value Analysis can be applied to both input or output values

4) There are two types of partitions, valid and invalid partitions

Valid partitions are expected outputs, the invalid partitions are unexpected i/o.

5) Equivalence partitioning coverage

This is calculated by: dividing the number of test cases to the total number of partitions.

If you have 5 partitions and you covered 4 with test cases, this is 4/5 or 80% coverage.

6) Boundary-Value Analysis coverage

The number of boundaries covered w/ testcases divided by the total number of  boundaries in the system.

7) Testing two values from the same partition doesn't increase the coverage

One test case per partition. 

8) Equivalence Partitioning = EP

9) Boundary-Value Analysis = BVA

## Decision table testing

Different combinations of conditions result in different outcomes.

Example: To get a discount at a store you must:

1) total cost of items must be above 100$

2) gold subscription

There is a formula to calculate how many possibilities of testing there are.

You take the amount of inputs possible (this can be, user logs in with username but no password for instance).
Then take the outputs possible (this can be the error message that appears or not.).
You take the input amount and take the power of it with the amount that is the output.

For example, you have 2 inputs, and 2 expected outputs. This means you have 2 to the power of 2, which equals 4. This means you have 4 test cases.

An example:

![](decision-table-conditions.png)


![](decision-testing-table.png)

## State transition testing

What is a state?
A state can be like on or off.

What is a transition? the transition is the part between on and off.